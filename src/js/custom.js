$(function(){
// chat toggle (maximize/minimze)
$(".chat-toggle").on("click", function(){
	$(this).parents(".chat-box").toggleClass("is-minimized");
});

$('[data-toggle="tooltip"]').tooltip();

// make participant list are dragable
//  $('.content-area tbody').sortable();

//  slick slider
$('.js-slides3').slick({
	slidesToShow: 1,
	autoplay: false,
	infinite: true,
	arrows: false,
	vertical: true,
	asNavFor: '.js-slides1, .js-slides2'
});
$('.js-slides2').slick({
	slidesToShow: 1,
	autoplay: false,
	infinite: true,
	asNavFor: '.js-slides1, .js-slides3'
});

$('.js-slides1').slick({
	slidesToShow: 6,
	infinite: true,
	autoplay: true,
	asNavFor: '.js-slides2, .js-slides3',
	centerMode: true,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 5
			}
		}
	] 
});
$('.js-stop-slick').on('click', function() {
    $('.slides').slick('slickPause');
    return false;
});
$('.slides-single').slick({
	slidesToShow: 6,
	infinite: false,
	responsive: [
		{
			breakpoint: 1400,
			settings: {
				slidesToShow: 5
			}
		}
	] 
});
var totSlide = $('.js-slides2 .slick-list .slick-track > div').length;
$('.total-slide').text(totSlide - 11);

$('.js-slides2').on('afterChange', function(event, slick, currentSlide){
  var slide_length = slick.$slides.length;
  var slide_length_true = slide_length - 1;
  //console.log(currentSlide);
  	$('.js-prev').removeClass('disabled');
  	if(currentSlide == 0) {
  		$('.js-prev').addClass('disabled');
  	}
  	if(currentSlide == slide_length_true) {
  		$('.js-next').addClass('disabled');
  	} else {
  		$('.js-next').removeClass('disabled');
  	}
  	$('.current-slide').text(currentSlide + 1);

  	var dataAttr = $('.js-slides2 .slick-list .slick-track .slick-current .slide-item').data('img')
  	$('.js-img').attr({
     src: 'dist/images/' + dataAttr + '.png'
  	});
});
$('.js-next').on('click', function(){
	$('.js-slides2 .slick-next').trigger('click');
});
$('.js-prev').on('click', function(){
	$('.js-slides2 .slick-prev').trigger('click');
});


// accordion
$('.accordion-header').on('click', function (){
	$(this).parent().toggleClass('is-active');
});
videoWidth();
$(window).resize(function(){
	if ($(window).width() <= 1280) {
		sliderWidth();
		videoWidth();
	}
});

function sliderWidth() {
	var sidebarWidth = 360;
	var windowWidth = $(window).width();
	var slideWidth = windowWidth - sidebarWidth;
	$('.fixet-bottom').css('max-width', slideWidth);
}
function videoWidth() {
	var loginWidth = $('.js-login').width();
	var windowWidth = $(window).width();
	var windowHeight = $(window).height();
	var vidWidth = windowWidth - loginWidth;
	$('#myVideo').css('width', vidWidth);
	$('#myVideo').css('height', windowHeight);
}

$('.js-menu').click(function(){
	$("#navbar-menu").toggleClass('hide');
    $(".content-area").toggleClass('menu-hide');
});
$('.js-side').click(function(){
	$(this).toggleClass('open');
	$(".sidebar-area").toggleClass('close-side');
	$(".content-area").toggleClass('close-side');
	
	// var windowWidth = $(window).width();
	// $('.js-bottom').css('max-width', windowWidth - 60);
	return false;

});

$('.timer').countimer({

  // In seconds : 0
  // In minutes: 1
  // In hours: 2
  //displayMode : displayMode.FULL,

  // Enable the timer events
  enableEvents: false,

  // Display the milliseconds next to the seconds in the full view
  displayMillis: false,

  // whether to remove the HTML element from the DOM after destroy
  destroyDOMElement: false,

  // Auto start on inti
  autoStart : true,

  // Show hours
  useHours : false,

  // Custom indicator for minutes
  minuteIndicator: '',

  // Custom indicator for seconds
  secondIndicator: '',

  // Separator between each time block
  separator : ':',

  // Number of leading zeros
  leadingZeros: 2,

  // Initial time
  initHours : 0,
  initMinutes : 0,
  initSeconds: 0

});

$('.pause').click(function(){
	// pause the timer
	$('.timer').countimer('stop');
	$(this).parent().hide();
	$('.play-btn').css({
		'display' : 'inline-block'
	});
	return false;
});
$('.play').click(function(){
	// pause the timer
	$('.timer').countimer('resume');
	$(this).parent().hide();
	$('.pause-btn').css({
		'display' : 'inline-block'
	});
	return false;
});
$('.refresh').click(function(){
	// pause the timer
	$('.timer').countimer('start');
	return false;
});

$('.js-collapsed').click(function(){
	var collapsed = $(this).data('collapse');
	$(collapsed).toggleClass('hide');
	$(this).toggleClass('open');
});
// start the timer
//$('.timer').countimer('start');

// resume the timer
//$('.timer').countimer('resume');
// $('.dropdown').on('shown.bs.dropdown', function(){

// 	var input = document.getElementById("mySettingKey");
// 	// Execute a function when the user releases a key on the keyboard
// 	input.addEventListener("keyup", function(event) {
// 		alert("teeessss");
// 	  // Number 13 is the "Enter" key on the keyboard
// 	  if (event.keyCode === 39) {
// 	    // Cancel the default action, if needed
// 	    event.preventDefault();
// 	    // Trigger the button element with a click
// 	    document.getElementById("jsNext").click();
// 	  }
// 	  if (event.keyCode === 37) {
// 	    // Cancel the default action, if needed
// 	    event.preventDefault();
// 	    // Trigger the button element with a click
// 	    document.getElementById("jsPrev").click();
// 	  }
// 	});
// });
$('.js-dropdown').click(function(){
	$(this).parent().toggleClass('open');
});

$('.btn-config').click(function(){
	$(this).parent('.my-option-list').find('.btn-config').removeClass('active');
	$(this).addClass('active');
});

$("#small").click(function(event){
    event.preventDefault();
    $(".notes p").animate({"font-size":"12px", "line-height":"18px"});
    
});
  
$("#medium").click(function(event){
    event.preventDefault();    
    $(".notes p").animate({"font-size":"14px", "line-height":"20px"});
    
});
  
$("#large").click(function(event){
    event.preventDefault();    
    $(".notes p").animate({"font-size":"18px", "line-height":"22px"});
    
});
  
$( ".js-font" ).click(function() {
	$(this).parent().find('a').removeClass("selected");
	$(this).addClass("selected");
});

$(".jsSelect").click(function(){
	$(this).parents('.form-group').find('.select').removeClass('select');
	$(this).parents('.radio-color').addClass('select');
});
  var openMobileMenu = function () {
    var body = $('body');
    if (body.hasClass('open')) {
      closeMobileMenu();
    } else {
      body.addClass('open');
    }
  }
  var closeMobileMenu = function () {
    $('body').removeClass('open');
  }
  var mobileMenu = $('.js-mobile-menu');
  $(document).on('click', '.js-LeftMenu', function () {
    openMobileMenu();
  });

  $(document).off('click.mobileMenu').on('click.mobileMenu', function (e) {
    var t = $(e.target);
    if (t.closest('.js-mobile-menu').length < 1 && t.closest('.js-LeftMenu').length < 1) {
      closeMobileMenu(mobileMenu);
    }
  });

  var openMobileMenuRight = function () {
    var body = $('body');
    if (body.hasClass('open-right')) {
      closeMobileMenuRight();
    } else {
      body.addClass('open-right');
    }
  }
  var closeMobileMenuRight = function () {
    $('body').removeClass('open-right');
  }
  var mobileMenuRight = $('.js-mobile-menu-right');
  $(document).on('click', '.js-RightMenu', function () {
    openMobileMenuRight();
  });

  $(document).off('click.mobileMenuRight').on('click.mobileMenuRight', function (e) {
    var t = $(e.target);
    if (t.closest('.js-mobile-menu-right').length < 1 && t.closest('.js-RightMenu').length < 1) {
      closeMobileMenuRight(mobileMenuRight);
    }
  });

  // create poll question
  //if($('#multipleChoise').prop('checked')) { alert("it's checked"); }
  $('.custom-radio').click(function() {
    if($('#multipleChoise').is(':checked')) {
      $('.wrapp-in').fadeIn();
    } else {
      $('.wrapp-in').fadeOut();
    }
  });

  $('.js-add').click(function(){
    if($('#colorBlue').is(':checked')) {
      $('.list-question .blue').addClass('selected');
    }
    if($('#colorRed').is(':checked')) {
      $('.list-question .red').addClass('selected');
    }
    if($('#colorPurple').is(':checked')) {
      $('.list-question .purple').addClass('selected');
    }
    if($('#colorGreen').is(':checked')) {
      $('.list-question .green').addClass('selected');
    }
    if($('#colorYellow').is(':checked')) {
      $('.list-question .yellow').addClass('selected');
    }
    if($('#colorBlack').is(':checked')) {
      $('.list-question .black').addClass('selected');
    }
    return false;
  });


  // $('.op-menu').on("click", function(e){
  //   $('body').append('<div class="back-drop"></div>');
  // });
  $(document).ready(function(){
    $('.dropdown-submenu a.submenu-toggle').on("click", function(e){
      $(this).next('ul').toggleClass('show');
      return false;
    });
    $('.js-dropdown').on("click", function(e){
      $('.dropdown-submenu').find('.dropdown-menu-right').removeClass('show');
      $('body').toggleClass('addDrop');
    });
    $(document).on('click', '.addDrop', function() {
      $('.dropdown').find('.dropdown-menu-right').removeClass('show');
      $('body').removeClass('addDrop');
    });
  });
  

});